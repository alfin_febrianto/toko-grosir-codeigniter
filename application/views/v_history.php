<h1><?= $title; ?></h1>
<br>
<br>
<center>
  <?php if ($this->session->flashdata('msg')): ?>
    <div class="btn btn-success">
      <?= $this->session->flashdata('msg'); ?>
    </div>
  <?php endif ?>
</center>
<br>
<table class="table table-hover table-striped" id="example">
	<thead>
		<tr>
			<td><center>NO.</td>
			<td><center>ID NOTA</center></td>
			<td><center>GRAND TOTAL</center></td>
			<td><center>STATUS</center></td>
			<td><center>CONFIRM</center></td>
			<td><center>DETAIL</center></td>
		</tr>
	</thead>
	<tbody>
		<?php $no = 0; foreach ($show as $s): $no++; ?>
			<tr>
				<td><center><?= $no; ?></center></td>
				<td><center><?= $s->id_nota; ?></center></td>
				<td><center><?= number_format($s->grandtotal); ?></center></td>
				<td><center><?= $s->status; ?></center></td>
				<td>
					<center>
					<?php if ($s->status!=null): ?>
						Lunas
					<?php else: ?>
						<a href="<?=base_url('index.php/history/confirm/'.$s->id_nota)?>">Confirm</a> | <a href="<?= base_url('index.php/history/del/'.$s->id_nota); ?>" onclick="return confirm('Are You Sure To Delete THe Order ?')">Delete</a>
					<?php endif ?>
					</center>
				</td>
				<td>
					<center>
						<a href="#detail<?= $s->id_nota; ?>" data-toggle="modal">Show</a>
					</center>


					<div class="modal fade" id="detail<?= $s->id_nota; ?>">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		        					<h4 class="modal-title">Detail Item</h4>
								</div>
								<div class="modal-body">
									<?php foreach($this->h->detail($s->id_nota) as $value): ?>
										<div class="row">
											<div class="col-md-3">
												<div class="thumbnail">
													<img src="<?= base_url('asset/gambar/barang/'.$value->gambar); ?>">
												</div>
											</div>
											<div class="col-md-9">
												<table class="table table-hover table-striped">
													<tr>
														<td>Item Name</td><td><?= $value->nama_barang; ?></td>
													</tr>
													<tr>
														<td>Category</td><td><?= $value->nama_kategori; ?></td>
													</tr>
													<tr>
														<td>Price</td><td><?= number_format($value->harga); ?></td>
													</tr>
												</table>
											</div>
											</div>
									<?php endforeach ?>
								</div>
								<div class="modal-footer">
									
								</div>
							</div>
						</div>
					</div>


				</td>
			</tr>
		<?php endforeach ?>

	</tbody>
</table>
<!-- modal menampilkan detail barang yang dipesan-->
<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
 
</script>