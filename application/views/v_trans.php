<center>
	<?php if ($this->session->flashdata('msg')): ?>
		<div class="btn btn-success">
			<?= $this->session->flashdata('msg'); ?>
		</div>
		<br>
	<?php endif ?>
</center>
<h2 align="center"><?= $title; ?></h2>
<br>
<div class="col-md-7">
	<table id="example" class="table table-hover table-striped">
		<thead>
			<tr>
				<th><center>NO.</center></th>
				<th><center>Name</center></th>
				<th><center>Price</center></th>
				<th><center>Category</center></th>
				<th><center>Action</center></th>
			</tr>
		</thead>
		<tbody>
			<?php $no=0; foreach ($lst as $b): $no++; ?>
				<tr>
					<td><center><?= $no; ?></center></td>
					<td><center><?= $b->nama_barang; ?></center></td>
					<td><center><?= $b->harga; ?></center></td>
					<td><center><?= $b->nama_kategori; ?></center></td>
					<td>
						<center>
							<a class="btn btn-info" href="<?=base_url('index.php/trans/addcart/'.$b->id_barang)?>">Pesan</a>
						</center>
					</td>
				</tr>
			<?php endforeach ?>
			
		</tbody>
	</table>
</div>
<div class="col-md-5">
<center>
	<a class="btn btn-danger" href="<?=base_url('index.php/trans/clearcart')?>" style="margin-bottom: 10px;">Clear Cart</a>
	<br>
	<?php if ($this->session->flashdata('msg1')): ?>
		<div class="btn btn-warning" style="margin-bottom: 10px;">
			<?= $this->session->flashdata('msg1'); ?>
		</div>
	<?php endif ?>
<form action="<?=base_url('index.php/trans/save')?>" method="post">
	<input type="text" name="nama_pembeli" class="form-control" placeholder="Nama Pembeli" style="margin-bottom: 10px;">
</center>
	<table class="table table-striped table-hover">
		<tr>
			<th><center>NO</center></th>
			<th><center>Name</center></th>
			<th><center>QTY</center></th>
			<th><center>Price</center></th>
			<th><center>Subtotal</center></th>
			<th><center>Action</center></th>
		</tr>
		<?php $no=0; foreach ($this->cart->contents() as $items): $no++; ?>
		<input type="hidden" name="id_barang[]" value="<?= $items['id']; ?>">
		<input type="hidden" name="rowid[]" value="<?= $items['rowid']; ?>">
			<tr>
				<td><?= $no; ?></td>
				<td><?= $items['name']; ?></td>
				<td>
					<input type="number" name="qty[]" value="<?= $items['qty']; ?>" class="form-control">
				</td>
				<td><?= $items['price']; ?></td>
				<td><?= number_format($items['subtotal']); ?></td>
					<input type="hidden" name="subtotal[]" value="<?= $items['subtotal']; ?>">
				<td>
					<a href="<?= base_url('index.php/trans/rm_cart/'.$items['rowid']); ?>" class="btn btn-danger">&times;</a>
				</td>
			</tr>
		<?php endforeach ?>
			<input type="hidden" name="grandtotal" value="<?= $this->cart->total(); ?>">
			<tr style="border-bottom:5px black solid">
				<th colspan="4">Grand Total</th>
				<th><?= number_format($this->cart->total()); ?></th>
				<th></th>
			</tr>
	</table>
	<input class="btn btn-success" type="submit" name="update" value="Update QTY">
	<input type="submit" onclick="return confirm('Are You Sure ?')" class="btn btn-danger" value="Pay" name="pay">
</form>
<?php if ($this->session->flashdata('msg')): ?>
	<div class="alert alert-warning"><?= $this->session->flashdata('msg'); ?></div>
<?php endif ?>
</div>

<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
 
</script>