<h2 align="center">Nota Pembelian</h2>
<center>
	No Nota : <?= $data->id_nota; ?><br>
	Tanggal Beli : <?= $data->tgl; ?><br>
	<br>
	<table border="1" style="border-collapse: collapse;">
		<tr>
			<th><center>NO</center></th>
			<th><center>Name</center></th>
			<th><center>Price</center></th>
			<th><center>QTY</center></th>
			<th><center>Subtotal</center></th>
		</tr>
		<?php $no=0; foreach ($this->t->detail_trans($data->id_nota) as $d): $no++;?>
			<tr>
				<th><center><?=$no?></th>
				<th><center><?=$d->nama_barang?></center></th>
				<th><center><?= number_format($d->harga)?></center></th>
				<th><center><?=$d->jumlah?></center></th>
				<th><center><?= number_format(($d->harga*$d->jumlah))?></center></th>
			</tr>
		<?php endforeach ?>
		<tr>
			<th colspan="4"><center>Grand Total</center></th>
			<th><center><?= number_format($data->grandtotal); ?></center></th>
		</tr>
	</table>
</center>

<!-- <script type="text/javascript">
	window.print();
	location.href="<?= base_url('index.php/trans'); ?>";
</script> -->