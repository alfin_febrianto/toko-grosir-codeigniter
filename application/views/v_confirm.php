<h1><?= $title; ?></h1>
<br>
<br>
<center>
  <?php if ($this->session->flashdata('msg')): ?>
    <div class="btn btn-success">
      <?= $this->session->flashdata('msg'); ?>
    </div>
  <?php endif ?>
</center>
<br>
<form action="<?=base_url('index.php/history/save_confirm')?>" method="post" enctype="multipart/form-data">
	<input type="hidden" name="id_nota" value="<?= $id_nota; ?>">
	<input type="file" name="bukti" class="form-control" style="width:200px;display: inline-block;margin-right: 20px">
	<input type="submit" name="confirm" value="Confirm" class="btn btn-danger">
</form>
<br>
<div class="col-md-5 col-md-offset-3">
	<div class="thumbnail">
	<?php if ($det_pesan->bukti!=null): ?>
		<img src="<?= base_url('asset/gambar/bukti/'.$det_pesan->bukti); ?>" >
	<?php else: ?>
		<img src="<?= base_url('asset/gambar/bukti/bukti.png'); ?>" >
	<?php endif ?>	
	</div>
</div>
