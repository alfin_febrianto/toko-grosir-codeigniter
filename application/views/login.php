﻿<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?= $title; ?></title>
    <!-- Favicon-->
    <link rel="icon" href="<?= base_url(); ?>/asset/login/favicon.ico" type="image/x-icon">

    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,700&subset=latin,cyrillic-ext" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" type="text/css">

    <!-- Bootstrap Core Css -->
    <link href="<?= base_url(); ?>/asset/login/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

    <!-- Waves Effect Css -->
    <link href="<?= base_url(); ?>/asset/login/plugins/node-waves/waves.css" rel="stylesheet" />

    <!-- Animation Css -->
    <link href="<?= base_url(); ?>/asset/login/plugins/animate-css/animate.css" rel="stylesheet" />

    <!-- Custom Css -->
    <link href="<?= base_url(); ?>/asset/login/css/style.css" rel="stylesheet">
</head>

    <body class="login-page">
        <div class="login-box">
            <div class="logo">
                <a href="javascript:void(0);" style="font-size: 32px;">Admin <b>GORCERY STORE</b></a>
                <small style="color: black;">Yang Tidak Berkepentingan DIlarang Masuk</small>
            </div>
            <div class="card">
                <div class="body">
                    <form id="signin" method="post" action="<?= base_url('index.php/admin/check_admin'); ?>">
                        <?php if ($this->session->flashdata('msg')) { ?>
                         <div class='alert alert-success' class="msg">
                             <?= $this->session->flashdata('msg'); ?>
                         </div>
                        <?php } ?>
                        <div class="input-group">
                            <div class="form-line">
                                <input type="text" class="form-control" name="username" placeholder="Username">
                                <span class="glyphicon glyphicon-user form-control-feedback"></span>
                            </div>
                        </div>
                        <div class="input-group">
                            <div class="form-line">
                                <input type="password" class="form-control" name="password" placeholder="Password">
                                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-8 p-t-5">
                                <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                                <label for="rememberme">Remember Me</label>
                            </div>
                            <div class="col-xs-4">
                                <input type="hidden" name="check" value="1">
                                 <button id="login" name="login1" class="btn btn-block bg-pink waves-effect">Log-In</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Jquery Core Js -->
        <script src="<?= base_url(); ?>/asset/login/plugins/jquery/jquery.min.js"></script>

        <!-- Bootstrap Core Js -->
        <script src="<?= base_url(); ?>/asset/login/plugins/bootstrap/js/bootstrap.js"></script>

        <!-- Waves Effect Plugin Js -->
        <script src="<?= base_url(); ?>/asset/login/plugins/node-waves/waves.js"></script>

        <!-- Validation Plugin Js -->
        <script src="<?= base_url(); ?>/asset/login/plugins/jquery-validation/jquery.validate.js"></script>

        <!-- Custom Js -->
        <script src="<?= base_url(); ?>/asset/login/js/admin.js"></script>
        <script src="<?= base_url(); ?>/asset/login/js/pages/examples/sign-in.js"></script>
    </body>
</html>