<h1><?= $title; ?></h1>
<br>
<a href="#add" data-toggle="modal" class="btn btn-primary">Add New Item</a>
<br>
<br>
<center>
	<?php if ($this->session->flashdata('msg')): ?>
		<div class="btn btn-success">
			<?= $this->session->flashdata('msg'); ?>
		</div>
	<?php endif ?>
</center>
<br>
<table id="example" class="table table-hover">
	<thead>
		<tr style="background-color: orange;">
			<td><center>ID List</center></td>
			<td><center>Item Name</center></td>
      <td><center>Image</center></td>
			<td><center>Price</center></td>
			<td><center>Nama Category</center></td>
			<td><center>Stock</center></td>
			<td><center>Action</center></td>
		</tr>
	</thead>
	<tbody>

		<?php foreach ($show as $l): ?>
		<tr style="background-color: white;">
			<td><center><?= $l->id_barang; ?></center></td>
			<td><center><?= $l->nama_barang; ?></center></td>
      <td><img src="<?= base_url('asset/gambar/barang/'.$l->gambar); ?>" style="width:40px">
			<td><center><?= $l->harga; ?></center></td>
			<td><center><?= $l->nama_kategori; ?></center></td>
			<td><center><?= $l->stock; ?></center></td>
			<td><center><a href="#edit" onclick="edit('<?=$l->id_barang?>')" data-toggle="modal" class="btn btn-success">Change</a> <a href="<?=base_url('index.php/lists/delete/'.$l->id_barang)?>" onclick="return confirm('Are Your Sure Delete This Item ?')" class="btn btn-danger">Delete</a></center></td>
		</tr>
		<?php endforeach ?>
		
	</tbody>
</table>
<div class="modal fade" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Add New Item</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/lists/add')?>" method="post" enctype="multipart/form-data">
      	<table>
      		<tr>
      			<td>Item Name</td><td><input type="text" name="item_name" required class="form-control"></td>
      		</tr>
          <tr>
            <td>Image</td><td><input type="file" name="image" class="form-control" required=""></td>
          </tr>
      		<tr>
      			<td>Price</td><td><input type="text" name="price" required class="form-control"></td>
      		</tr>
      		<tr>
            <td>Nama Kategori</td><td><select name="nama_kategori" class="form-control" required>
              <option></option>
              <?php foreach ($cat as $c): ?>
                <option value="<?= $c->id_kategori; ?>">
                  <?= $c->nama_kategori; ?></option>
              <?php endforeach ?>
            </select></td>
      		</tr>
      		<!-- <tr>
      			<td>Photo</td><td><input type="file" name="photo" required class="form-control"></td>
      		</tr> -->
      		<tr>
      			<td>Stock</td><td><input type="number" class="form-control" value="0" name="stock">
      		</tr>
      	</table>
      	<input type="submit" name="add" value="Add" class="btn btn-success">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Edit Item</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/lists/update')?>" method="post">
        <table>
          <tr>
            <input type="hidden" name="id_barang" id="id_barang">
            <td>Item Name</td><td><input type="text" name="item_name" id="item_name" required class="form-control"></td>
          </tr>
          <tr>
            <td>Image</td><td><input type="file" name="e_image" class="form-control"></td>
          </tr>
          <tr>
            <td>Price</td><td><input type="text" name="price" id="price" required class="form-control"></td>
          </tr>
          <tr>
            <td>Nama Kategori</td>
            <td>
              <select name="nama_kategori" class="form-control" id="nama_kategori" required>
                <option></option>
                <?php foreach ($cat as $c): ?>
                  <option value="<?= $c->id_kategori; ?>">
                    <?= $c->nama_kategori; ?></option>
                <?php endforeach ?>
              </select>
            </td>
          </tr>
          <!-- <tr>
            <td>Photo</td><td><input type="file" name="photo" required class="form-control"></td>
          </tr> -->
          <tr>
            <td>Stock</td><td><input type="number" class="form-control" name="stock" id="stock">
          </tr>
        </table>
        <input type="submit" name="edit" value="Save" class="btn btn-success">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
  function edit(a){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/lists/detail_edit/"+a, 
       dataType:"json",
       success:function(data){
        $("#id_barang").val(data.id_barang);
        $("#item_name").val(data.nama_barang);
        $("#price").val(data.harga);
        $("#nama_kategori").val(data.id_kategori);
        $("#stock").val(data.stock);
      }
      });
    }
</script>