<h1><?= $title; ?></h1>
<br>
<a href="#add" data-toggle="modal" class="btn btn-primary">Add New Category</a>
<br>
<br>
<center>
  <?php if ($this->session->flashdata('msg')): ?>
    <div class="btn btn-success">
      <?= $this->session->flashdata('msg'); ?>
    </div>
  <?php endif ?>
</center>
<br>
<table id="example" class="table table-hover">
	<thead>
		<tr style="background-color: orange;">
			<td><center>ID Category</center></td>
      <td><center>Category Name</center></td>
      <td><center>Action</center></td>
		</tr>
	</thead>
	<tbody>

		<?php foreach ($show as $c): ?>
		<tr style="background-color: white;">
			<td><center><?=$c->id_kategori?></center></td>
      <td><center><?=$c->nama_kategori?></td></center>
      <td><center><a href="#edit" onclick="edit('<?=$c->id_kategori?>')" data-toggle="modal" class="btn btn-success">Change</a> <a href="<?=base_url('index.php/category/delete/'.$c->id_kategori)?>" onclick="return confirm('Are Your Sure Delete This Category ?')" class="btn btn-danger">Delete</a></center></td>
		</tr>
		<?php endforeach ?>
		
	</tbody>
</table>
<div class="modal fade" id="add">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Add Category</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/category/add')?>" method="post">
      	<table>
      		<tr>
      			<td>Category Name</td><td><input type="text" name="nama_kategori" required class="form-control"></td>
      		</tr>
      	</table>
      	<input type="submit" name="add" value="Add" class="btn btn-success">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div class="modal fade" id="edit">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title">Category Edit</h4>
      </div>
      <div class="modal-body">
       <form action="<?=base_url('index.php/category/update')?>" method="post">
        <table>
          <tr>
            <input type="hidden" id="id_kategori" name="id_kategori">
            <td>Category Name</td><td><input type="text" id="nama_kategori" name="nama_kategori" required class="form-control"></td>
          </tr>
        </table>
        <input type="submit" name="edit" value="Save" class="btn btn-success">
        </form>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script type="text/javascript">
  $(document).ready(function(){
    $('#example').DataTable();
  });
  function edit(i){
      $.ajax({
       type:"post",
       url:"<?=base_url()?>index.php/category/detail_edit/"+i, 
       dataType:"json",
       success:function(data){
        $("#id_kategori").val(data.id_kategori);
        $("#nama_kategori").val(data.nama_kategori);
      }
      });
    }
</script>