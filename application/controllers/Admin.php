<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('m_admin', 'a');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['title'] = "--- Home ---";
			$data['content'] = "home";
			$this->load->view('index', $data, FALSE);
		} else {
			redirect('admin/login','refresh');
		}
	}

	public function login()
	{
		if ($this->session->userdata('logged_in') != TRUE) {
			$data['title']="--- Login ---";
			$this->load->view('login', $data, FALSE);
		} else {
			redirect('admin','refresh');
		}
	}

	public function check_admin()
	{
		if($this->input->post('check')){
			$this->form_validation->set_rules('username', 'Username', 'trim|required');
			$this->form_validation->set_rules('password', 'Password', 'trim|required');
			if ($this->form_validation->run() == TRUE) {	
				if ($this->a->check_user()->num_rows() > 0) {
					$data = $this->a->check_user()->row();
					$array = array(
						'logged_in' => TRUE,
						'id_admin' 	=> $data->id_admin,
						'nama_admin'=> $data->nama_admin,
					);
					
					$this->session->set_userdata( $array );
					redirect('admin/index','refresh');
				} else {
					$this->session->flashdata('msg', 'Username Atau Password Salah');
					redirect('admin/login');
				}
			} else {
				$this->session->flashdata('msg', validation_errors());
				redirect('admin/login');
			}
		}
	}

	public function logout()
	{
		$array = array(
			'logged_in' => FALSE,
		);
		$this->session->set_userdata($array);
		session_destroy();
		redirect('admin','refresh');
	}


}

/* End of file Admin.php */
/* Location: ./application/controllers/Admin.php */