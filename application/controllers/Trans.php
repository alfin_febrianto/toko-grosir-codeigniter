<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Trans extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_trans', 't');
		$this->load->model('m_list', 'l');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['lst']=$this->l->show_lst();
			$data['title']="--- Transactions ---";
			$data['content']="v_trans";
			$this->load->view('index', $data, FALSE);
		} else {
			redirect('admin/login','refresh');
		}
	}

	public function addcart($id)
	{
			$detail=$this->l->detail($id);

			$data = array(
				'id'      => $detail->id_barang,
				'qty'     => 1,
				'price'   => $detail->harga,
				'name'    => $detail->nama_barang,
				'options' => array('kategori'=>$detail->nama_kategori)
			);
			
			$this->cart->insert($data);
			redirect('trans','refresh');
	}

	public function clearcart()
	{
		$this->cart->destroy();
		redirect('trans','refresh');
	}

	public function rm_cart($idc)
	{
		$update = array(
			'rowid' => $idc,
			'qty'	=> 0
		);
		
		$this->cart->update($update);
		redirect('trans','refresh');
	}

	public function save()
	{
		if ($this->input->post('update')) {
			for($i=0;$i<count($this->input->post('rowid'));$i++){
				$data = array(
					'rowid' => $this->input->post('rowid')[$i],
					'qty'   => $this->input->post('qty')[$i]
				);
				$this->cart->update($data);
			}
			redirect('trans','refresh');		
		} elseif($this->input->post('pay')){
			$this->form_validation->set_rules('nama_pembeli', 'Pembeli', 'trim|required');
			if ($this->form_validation->run() == TRUE) {
				$id = $this->t->save_cart_db();
				if($id){
					$data['data']=$this->t->detail();
					// $this->load->view('v_nota', $data, FALSE);
					$data['title']="--- NOTA ---";
					$data['content']="v_nota";
					$this->load->view('index', $data, FALSE);
				}
			} else {
				$this->session->set_flashdata('msg1', validation_errors());
				redirect('trans','refresh');
			}	
		}

	}

}

/* End of file Trans.php */
/* Location: ./application/controllers/Trans.php */