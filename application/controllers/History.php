<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class History extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_history', 'h');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['show']=$this->h->show_h();
			$data['title']="--- History ---";
			$data['content']="v_history";
			$this->load->view('index', $data, FALSE);
		} else {
			redirect('admin/login','refresh');
		}
	}

	public function del($id)
	{
		if($this->h->del($id)){
			$this->session->set_flashdata('msg', 'Delete Success');
			redirect('history','refresh');
		} else {
			$this->session->set_flashdata('msg', 'Cannot Delete');
			redirect('history','refresh');
		}

	}

	public function confirm($id)
	{
		$data['id_nota'] = $id;
		$data['det_pesan'] = $this->h->confirm($id);
		$data['title'] = "--- Confirmation ---";
		$data['content'] = "v_confirm";
		$this->load->view('index', $data, FALSE);
	}

	public function save_confirm()
	{
		if($this->input->post('confirm')){

			$config['upload_path'] = './asset/gambar/bukti/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '3000';
			$config['max_width']  = '3000';
			$config['max_height']  = '3000';
			
			$this->load->library('upload', $config);
			
			if ( ! $this->upload->do_upload('bukti')){
				$error = $this->upload->display_errors();
				$this->session->set_flashdata('msg', $error);
				redirect('history/confirm/'.$this->input->post('id_nota'),'refresh');
			}
			else{
					$proses=$this->h->save_db($this->input->post('id_nota'),$this->upload->data('file_name'));
					if($proses){						
						$this->session->set_flashdata('msg', 'Success');
						redirect('history/confirm/'.$this->input->post('id_nota'),'refresh');
					} else {
						$this->session->set_flashdata('msg', 'Failed');
						redirect('history/confirm/'.$this->input->post('id_nota'),'refresh');
					}
				
			}
		}
	}

}

/* End of file History.php */
/* Location: ./application/controllers/History.php */