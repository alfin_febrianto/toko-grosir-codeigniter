<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Lists extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_list', 'l');
		$this->load->model('m_category', 'c');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['show']=$this->l->show_lst();
			$data['cat']=$this->c->show_cat();
			$data['title']="--- List ---";
			$data['content']="v_list";
			$this->load->view('index', $data, FALSE);
		} else {
			redirect('admin/login','refresh');
		}
	}

	public function add()
	{
		if ($this->input->post('add')) {
			$config['upload_path'] = './asset/gambar/barang/';
			$config['allowed_types'] = 'gif|jpg|png';
			$config['max_size']  = '3000';
			$config['max_width']  = '3000';
			$config['max_height']  = '3000';
			$this->load->library('upload', $config);
			if (!$this->upload->do_upload('image')){
				$this->session->set_flashdata('msg', $this->upload->display_errors());
				redirect('lists','refresh');
			} else {
				$uploadd = $this->l->add_list($this->upload->data('file_name'));
				if($uploadd){
					$this->session->set_flashdata('msg', 'New Item Added');
					redirect('lists','refresh');
				} else {
					$this->session->set_flashdata('msg', 'New Item Cannot Added');
					redirect('lists','refresh');
				}
			}
		}
	}

	public function detail_edit($ide)
	{
		$data = $this->l->detail($ide);	
		echo json_encode($data);
	}

	public function update()
	{
		if ($this->l->save()) {
			$this->session->set_flashdata('msg', 'Item Edited');
			redirect('lists','refresh');
		} else {
			$this->session->set_flashdata('msg', 'Item Cannot Edited');
			redirect('lists','refresh');
		}

	}

	public function delete($idd)
	{
		if ($this->l->delete($idd)) {
			$this->session->set_flashdata('msg', 'Item Deleted');
			redirect('lists','refresh');
		} else {
			$this->session->set_flashdata('msg', 'Item Cannot Delete');
			redirect('lists','refresh');
		}
	}
	

}

/* End of file Lists.php */
/* Location: ./application/controllers/Lists.php */