<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

	public function __construct()
	{
		parent:: __construct();
		$this->load->model('m_category', 'c');
	}

	public function index()
	{
		if ($this->session->userdata('logged_in') == TRUE) {
			$data['show'] = $this->c->show_cat();
			$data['title']="--- Category ---";
			$data['content']="v_category";
			$this->load->view('index', $data, FALSE);
		} else {
			redirect('admin/login','refresh');
		}
	}

	public function add()
	{
		if($this->input->post('add')){
			if($this->c->add_cat()){
				$this->session->set_flashdata('msg', 'Success Add New Category');
				redirect('category','refresh');
			} else {
				$this->session->set_flashdata('msg', 'Cannot Add New Category');
				redirect('category','refresh');
			}
		}
	}

	public function detail_edit($ide)
	{
		$data = $this->c->detail($ide);
		echo json_encode($data);
	}

	public function update()
	{
		if ($this->input->post('edit')) {
			if ($this->c->save()) {
				$this->session->set_flashdata('msg', 'Success Edit Category');
				redirect('category','refresh');
			} else {
				$this->session->set_flashdata('msg', 'Cannot Edit Category');
				redirect('category','refresh');
			}
		}
	}

	public function delete($id)
	{
		if ($this->c->delete($id)) {
			$this->session->set_flashdata('msg', 'Success Delete Category');
			redirect('category','refresh');
		} else {
			$this->session->set_flashdata('msg', 'Cannot Delete Category');
			redirect('category','refresh');
		}
	}

}

/* End of file Category.php */
/* Location: ./application/controllers/Category.php */