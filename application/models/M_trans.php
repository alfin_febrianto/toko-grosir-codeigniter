<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_trans extends CI_Model {

	public function save_cart_db()
	{
		$object=array(
			'nama_pembeli'=>$this->input->post('nama_pembeli'),
			'tgl'=>date('Y-m-d'),
			'grandtotal'=>$this->input->post('grandtotal'),
			'status'=>'',
			'bukti'=>'-'
		);
		$this->db->insert('nota', $object);

		$tm_nota=$this->db->order_by('id_nota','desc')
					  ->where('nama_pembeli',$this->input->post('nama_pembeli'))
					  ->limit(1)
					  ->get('nota')
					  ->row();

		for($i=0;$i<count($this->input->post('rowid'));$i++){
			$hasil[]=array(
				'id_nota'	=> $tm_nota->id_nota,
				'id_barang'	=> $this->input->post('id_barang')[$i],
				'jumlah'	=> $this->input->post('qty')[$i],
				'subtotal'	=> $this->input->post('subtotal')[$i]
			);		
		}
		$proses=$this->db->insert_batch('transaksi', $hasil);

		if($proses){
			$this->cart->destroy();
			return $tm_nota->id_nota;
		} else {
			return 0;
		}
	}

	public function detail()
	{
		return $this->db->where('nama_pembeli', $this->input->post('nama_pembeli'))
					->join('transaksi','transaksi.id_nota = nota.id_nota')
					->get('nota')
					->row();
	}

	public function detail_trans($id)
	{
		return $this->db->where('id_nota',$id)
					->join('barang','barang.id_barang = transaksi.id_barang')
					->get('transaksi')->result();
	}

}

/* End of file M_trans.php */
/* Location: ./application/models/M_trans.php */