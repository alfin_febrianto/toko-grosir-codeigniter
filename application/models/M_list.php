<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_list extends CI_Model {

	public function show_lst()
	{
		return $this->db->join('kategori','kategori.id_kategori = barang.id_kategori')
						->get('barang')->result();
	}

	public function add_list($name_file)
	{
		// if($name!=""){
			$data = array(
				'nama_barang' => $this->input->post('item_name'),
				'harga' => $this->input->post('price'),
				'id_kategori' => $this->input->post('nama_kategori'),
				'stock' => $this->input->post('stock'),
				'gambar'=> $name_file,
			);
		// } else{
		// 	$data=array(
		// 		'nama_barang' => $this->input->post('item_name'),
		// 		'harga' => $this->input->post('price'),
		// 		'id_kategori' => $this->input->post('nama_kategori'),
		// 		'stock' => $this->input->post('stock'),
		// 	);
		// }

		return $this->db->insert('barang', $data);
	}

	public function detail($ide)
	{
		return $this->db->join('kategori','kategori.id_kategori=barang.id_kategori')
						->where('id_barang', $ide)
						->get('barang')
						->row();
	}

	public function save()
	{
		$update = array(
			'nama_barang' => $this->input->post('item_name'),
			'harga' => $this->input->post('price'),
			'id_kategori' => $this->input->post('nama_kategori'),
			'stock' => $this->input->post('stock')
		);

		return $this->db->where('id_barang', $this->input->post('id_barang'))
						->update('barang', $update);
	}

	public function delete($id)
	{
		return $this->db->where('id_barang', $id)
						->delete('barang');
	}

}

/* End of file M_list.php */
/* Location: ./application/models/M_list.php */