<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_admin extends CI_Model {

	public function check_user()
	{
		return $this->db
					->where('username', $this->input->post('username'))
					->where('password', md5($this->input->post('password')))
					->get('admin');
	}

}

/* End of file M_admin.php */
/* Location: ./application/models/M_admin.php */