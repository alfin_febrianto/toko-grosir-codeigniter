<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_category extends CI_Model {

	public function show_cat()
	{
		return $this->db->get('kategori')->result();
	}	

	public function add_cat(){
		$object=array(
			'nama_kategori'=>$this->input->post('nama_kategori'),
		);
		return $this->db->insert('kategori', $object);
	}

	public function detail($i)
	{
		return $this->db->where('id_kategori', $i)
						->get('kategori')
						->row();
	}

	public function save()
	{
		$update = array(
			'nama_kategori' => $this->input->post('nama_kategori')
		);

		return $this->db->where('id_kategori', $this->input->post('id_kategori'))
						->update('kategori', $update);
	}

	public function delete($id)
	{
		return $this->db->where('id_kategori', $id)->delete('kategori');
	}

}

/* End of file M_category.php */
/* Location: ./application/models/M_category.php */