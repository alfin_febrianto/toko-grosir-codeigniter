<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_history extends CI_Model {

	public function show_h()
	{
		return $this->db
					->get('nota')
					->result();
	}

	public function del($id)
	{
		$del_t = $this->db
					->where('id_nota',$id)
					->delete('transaksi');
		if($del_t){
			$del_n=$this->db->where('id_nota',$id)
						   ->delete('nota');
			if($del_n){	
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function detail($id_nota)
	{
		return $this->db
					->where('id_nota',$id_nota)
					->join('barang','barang.id_barang = transaksi.id_barang')
					->join('kategori','kategori.id_kategori = barang.id_kategori')
					->get('transaksi')
					->result();
	}

	public function confirm($id)
	{
		return $this->db
					->where('id_nota',$id)
					->get('nota')
					->row();
	}

	public function save_db($id,$nama_file)
	{
		$data = array(
			'bukti'=>$nama_file,
		);
		return $this->db
					->where('id_nota',$id)
					->update('nota', $data);
	}

}

/* End of file M_history.php */
/* Location: ./application/models/M_history.php */